# CocosCreatorGooglePlaySDK

#### 介绍
CocosCreator接入GooglePlay平台SDK示例

#### 说明
Cocos Creator 2.4.3
Target API Level 29
'com.google.android.instantapps:instantapps:1.1.0'
'com.google.android.gms:play-services-instantapps:17.0.0'
'com.google.android.gms:play-services-games:20.0.1'
'com.google.android.gms:play-services-ads:17.1.1'
'com.google.android.gms:play-services-auth:17.0.0'
'com.google.android.play:core:1.9.0'

#### 功能

1.  登录-LogInManager
2.  保存游戏进度-SavedGamesManager&SnapshotCoordinator
3.  排行榜-LeaderboardManager
4.  活动（事件打点）-EventManager
5.  广告（Admod，Banner&激励视频）-AdManager
6.  录屏分享（YouTube）-VideoRecordingMgr
7.  游戏内评论-ReviewMgr

#### 其他
这应该能极大的减少大家的开发接入时间。
GooglePlay常用的SDK功能，这里除了成就功能之外应该都涉及到。

另外，利用这套接入代码的游戏已在GooglePlay上线，欢迎大家体验，下面无耻的放下了链接，大家有条件的欢迎体验：
https://play.google.com/store/apps/details?id=org.cocos2d.ShunLiu

PS：感谢这位大哥，大哥快去围观他的帖子：
https://blog.csdn.net/oyh6857451/article/details/88596627
