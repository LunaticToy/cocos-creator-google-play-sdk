package org.cocos2dx.javascript;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AdManager implements RewardedVideoAdListener {
    
    private static final String AD_BANNER_UNIT_ID = "ca-app-pub-3940256099942544/6300978111";//横幅广告ID
    private static final String AD_VIDEO_ID = "ca-app-pub-3940256099942544/5224354917";//激励视频广告ID
    private static final String APP_ID = "ca-app-pub-3940256099942544~3347511713";//应用ID

    private InstantAppActivity mainActive = null;
    private static AdManager mInstance = null;

    private AdView mAdView;
    private LinearLayout bannerLayout;

    private RewardedVideoAd rewardedVideoAd;

    private static String curTag = "";
    private static final String TAG = "AdManager";

    public static AdManager getInstance() {
        if (null == mInstance) {
            mInstance = new AdManager();
        }
        return mInstance;
    }

    public void init(InstantAppActivity context) {
        Log.d(TAG, "init");
        this.mainActive = context;

        //初始化广告 SDK.
        MobileAds.initialize(context, APP_ID);

        //预加载激励视频广告
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        rewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();

        //预加载banner广告
        loadBannerAd();
    }

    /*
       加载google banner广告
     */
    public void loadBannerAd() {
        bannerLayout = new LinearLayout(this.mainActive);
        bannerLayout.setOrientation(LinearLayout.VERTICAL);

        // Create a banner ad. The ad size and ad unit ID must be set before calling loadAd.
        mAdView = new AdView(this.mainActive);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(AD_BANNER_UNIT_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        bannerLayout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        InstantAppActivity activity = (InstantAppActivity) this.mainActive;
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM;
        activity.addContentView(bannerLayout, params);
        bannerLayout.setVisibility(View.INVISIBLE);
    }

    /*
       显示google banner广告
     */
    public static void showBannerAd() {
        InstantAppActivity mActivity = (InstantAppActivity) AdManager.getInstance().mainActive;
        //一定要确保在UI线程操作
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdManager.getInstance().bannerLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    /*
       隐藏google banner广告
     */
    public static void hideBannerAd() {
        InstantAppActivity mActivity = (InstantAppActivity) AdManager.getInstance().mainActive;
        //一定要确保在UI线程操作
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdManager.getInstance().bannerLayout.setVisibility(View.INVISIBLE);
            }
        });
    }

    /*
    预加载google视频广告
     */
    public void loadRewardedVideoAd() {
        Log.d(TAG, "loadRewardedVideoAd");
        if (!rewardedVideoAd.isLoaded()) {
            Log.d(TAG, "!rewardedVideoAd.isLoaded()");
            rewardedVideoAd.loadAd(AD_VIDEO_ID, new AdRequest.Builder().build());
        }
    }

    /*
    显示视频广告
     */
    public static void showRewardedVideo(String param) {
        Log.d(TAG, "showRewardedVideo");

        curTag = param;
        InstantAppActivity mActivity = (InstantAppActivity) AdManager.getInstance().mainActive;
        //一定要确保在UI线程操作
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (AdManager.getInstance().rewardedVideoAd.isLoaded()) {
                    Log.d(TAG, "isLoaded then show");
                    AdManager.getInstance().rewardedVideoAd.show();
                }
            }
        });
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d(TAG, "onRewardedVideoAdLeftApplication");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d(TAG, "onRewardedVideoAdClosed");
        //预加载下一个视频广告
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        Log.d(TAG, "onRewardedVideoAdFailedToLoad: " + errorCode);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d(TAG, "onRewardedVideoAdLoaded");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d(TAG, "onRewardedVideoAdOpened");
    }

    @Override
    public void onRewarded(RewardItem reward) {
        Log.d(TAG, "onRewarded! currency: " + reward.getType() + "  amount: " + reward.getAmount());

        this.mainActive.InfoToJs("ADRewarded", curTag);
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d(TAG, "onRewardedVideoStarted");
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.d(TAG, "onRewardedVideoCompleted");
    }

    public void onResume() {
        mAdView.resume();
        rewardedVideoAd.resume(this.mainActive);
    }

    public void onPause() {
        mAdView.pause();
        rewardedVideoAd.pause(this.mainActive);
    }

    public void onDestroy() {
        mAdView.destroy();
        rewardedVideoAd.destroy(this.mainActive);
    }
}