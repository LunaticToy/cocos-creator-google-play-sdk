package org.cocos2dx.javascript;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.tasks.OnSuccessListener;

public class LeaderboardManager {

    private static LeaderboardManager mInstance = null;

    public static LeaderboardManager getInstance() {
        if (null == mInstance) {
            mInstance = new LeaderboardManager();
        }
        return mInstance;
    }

    // 排行榜
    private static final String TAG = "LeaderboardManager";
    private static InstantAppActivity this_tmp;
    //排行榜分数
    private static int score;

    public void init(InstantAppActivity context) {
        Log.d(TAG, "init");
        this.this_tmp = context;
    }

    /**
     * 提交分数到排行榜
     */
    public static void uploadScore(String leaderboard_id, int value) {
        if (!LogInManager.getInstance().isSignedIn())
            return;
        Games.getLeaderboardsClient(this_tmp, GoogleSignIn.getLastSignedInAccount(this_tmp)).submitScore(leaderboard_id, value);
    }

    /**
     * 显示排行榜
     */
    public static void showLeaderboard(String leaderboard_id) {
        Games.getLeaderboardsClient(this_tmp, GoogleSignIn.getLastSignedInAccount(this_tmp))
                .getLeaderboardIntent(leaderboard_id)
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        this_tmp.startActivityForResult(intent, 9004);
                    }
                });
    }

    /**
     * 得到排行榜数据
     */
    public static void getScoreData(String leaderboard_id) {
        if (!LogInManager.getInstance().isSignedIn())
            return;
        Games.getLeaderboardsClient(this_tmp, GoogleSignIn.getLastSignedInAccount(this_tmp))
                .loadCurrentPlayerLeaderboardScore(
                        leaderboard_id, LeaderboardVariant.TIME_SPAN_ALL_TIME,
                        LeaderboardVariant.COLLECTION_PUBLIC)
                .addOnSuccessListener(this_tmp, new OnSuccessListener<AnnotatedData<LeaderboardScore>>() {
                            public long score_tmp;

                            @Override
                            public void onSuccess(AnnotatedData<LeaderboardScore> leaderboardScoreAnnotatedData) {
                                if (leaderboardScoreAnnotatedData != null) {
                                    if (leaderboardScoreAnnotatedData.get() != null) {
                                        score_tmp = leaderboardScoreAnnotatedData.get().getRawScore();
                                        score = (int) score_tmp;
                                        Log.d(TAG, "score_tmp" + String.valueOf(score));
                                    }
                                }
                            }
                        }
                );
    }

    /**
     * 得到排行榜分数
     */
    public static int getScore() {
        Log.d(TAG, "score" + String.valueOf(score));
        return score;
    }
}
