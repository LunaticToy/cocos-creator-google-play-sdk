package org.cocos2dx.javascript;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnSuccessListener;

public class VideoRecordingMgr {

    private static String TAG = "VideoRecordingMgr";
    private InstantAppActivity mainActive = null;

    private static VideoRecordingMgr mInstance = null;

    public static VideoRecordingMgr getInstance() {
        if (null == mInstance) {
            mInstance = new VideoRecordingMgr();
        }
        return mInstance;
    }

    public void init(InstantAppActivity context) {
        Log.d(TAG, "init");
        this.mainActive = context;
    }

    private static final int RC_VIDEO_OVERLAY = 9011;

    private void showVideoOverlay(View myview) {
        Games.getVideosClient(this.mainActive, GoogleSignIn.getLastSignedInAccount(this.mainActive))
                .getCaptureOverlayIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        mainActive.startActivityForResult(intent, RC_VIDEO_OVERLAY);
                    }
                });
    }

    public static void showVideoOverlay() {
        Log.d(TAG, "开始录制！");
        VideoRecordingMgr.getInstance().showVideoOverlay(null);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_VIDEO_OVERLAY) {
            Log.d(TAG, "onActivityResult！");
        }
    }

}
